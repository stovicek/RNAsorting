# The compression of the reference database

In order to speed up the analysis, the reference database needs to be compressed by the `cmpress` program, which produces multiple binary files and that should hopefully speed things up.

```Shell
cmpress LSU_rRNA_bacteria.cm
```

## Already compressed reference databases

- [x] LSU_rRNA_bacteria.cm
- [x] LSU_rRNA_archaea.cm
- [ ] LSU_rRNA_eukarya.cm

- [x] SSU_rRNA_bacteria.cm
- [x] SSU_rRNA_archaea.cm
- [x] SSU_rRNA_eukarya.cm
- [x] SSU_rRNA_microsporidia.cm
