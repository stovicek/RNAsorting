import sys
import os
import subprocess
import argparse
from Bio import SeqIO

parser = argparse.ArgumentParser()

parser = argparse.ArgumentParser(description='# This program is designed to parse the input sequencing file and send it to execute on multiple CPUs.')

parser.add_argument('--in', dest='IN', help='Input database in .fastq format.')
parser.add_argument('--out', dest='OUT', help='Output file name.')
parser.add_argument('--outpath', dest='OUTPATH', help='Output path.')
parser.add_argument('--cm', dest='CM', help='Covariance model database.')
parser.add_argument('--cpu', dest='CPU', help='Number of CPU that should be used.')

args = parser.parse_args()

os.chdir("totalRNA2015")

os.system("cmsearch " + args.CM + " --noali " + args.IN + " > " + args.OUTPATH + '/' + args.OUT + ".table")
