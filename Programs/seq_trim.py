from Bio import SeqIO
import argparse

parser = argparse.ArgumentParser(description='# This program trims sequencing file.')

parser.add_argument('-i','--in', dest='IN', help='Input database in .fastq format.')
parser.add_argument('-o','--out', dest='OUT', help='Output file name.')
parser.add_argument('-p','--prefix', dest='PRE', help='Length of a prefix that is supposed to be trimmed.')
parser.add_argument('-s','--suffix', dest='SUF', help='Length of a suffix that is supposed to be trimmed.')
parser.add_argument('-m','--min', dest='MIN', help='Minimum length of sequence.')

args = parser.parse_args()

output = []

if args.SUF == None and args.PRE != None:
    # Just prefix
    for record in SeqIO.parse(args.IN, "fastq"):
        record = record[int(args.PRE):]
        if len(record) > int(args.MIN):
            output.append(record)
if args.PRE == None and args.SUF != None:
    # Just suffix
    for record in SeqIO.parse(args.IN, "fastq"):
        record = record[:-int(args.SUF)]
        if len(record) > int(args.MIN):
            output.append(record)
if args.SUF == None and args.PRE == None:
    # Neither suffix nor prefix required
    for record in SeqIO.parse(args.IN, "fastq"):
        if len(record) > int(args.MIN):
            output.append(record)
if args.SUF != None and args.PRE != None:
    # Both suffix and prefix required
    record = record[int(args.PRE):-int(args.SUF)]
    for record in SeqIO.parse(args.IN, "fastq"):
        if len(record) > int(args.MIN):
            output.append(record)

SeqIO.write(output,args.OUT,'fastq')
