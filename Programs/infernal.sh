#!/bin/bash
#$ -M no.drux@gmail.com
#$ -m beas

source ~/.cshrc

sed -n '1~4s/^@/>/p;2~4p' ~/fastspace/totalRNA2015/R2_trimmed3.fastq > ~/fastspace/totalRNA2015/R2_trimmed3.fasta

python3.5 ~/fastspace/totalRNA2015/infernal_singlecore.py --in R2_trimmed3.fasta --out r2 --outpath results/ssu_eukarya/ --cm databases/ssu_eukarya/SSU_rRNA_eukarya.cm --cpu 1
