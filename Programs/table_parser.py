import argparse
import re
from Bio import SeqIO

parser = argparse.ArgumentParser(description='# This program parses a cmsearch table, retrieves relevant sequences from a supplemented sequence file and saves  them in an output file.')

parser.add_argument('-t','--table', dest='TAB', help='Input table of assigned sequences.')
parser.add_argument('-i','--in', dest='IN', help='Input sequencing file in .fastq format.')
parser.add_argument('-o','--out', dest='OUT', help='Output file name.')
parser.add_argument('-u','--unassigned', dest='UN', help='The unassigned rest of the sequencing file.')
parser.add_argument('-tr','--treshold', dest='TRE', help='Maximum treshold of match E score for passing sequences.')

args = parser.parse_args()

seq_dictionary = {}

i = 0
with open(args.TAB, 'r') as input_handle:
    for line in input_handle:
        # Leafs through the header
        if i < 16:
            i += 1
            continue
        if line[1] == '-':
            break
        line = line.split()
        print(float(line[2]) < 0.001)
        float(line[2])
        seq_dictionary[line[5]] = float(line[2])
        i += 1

output = []

for record in SeqIO.parse(args.IN, "fastq"):
    if record.name in seq_dictionary:
        output.append(record)

SeqIO.write(output,args.OUT,'fastq')

output2 = []

for record in SeqIO.parse(args.IN, "fastq"):
    if record.name not in seq_dictionary:
        output2.append(record)

SeqIO.write(output2,args.UN,'fastq')
