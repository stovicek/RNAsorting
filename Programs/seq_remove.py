from Bio import SeqIO
import argparse

parser = argparse.ArgumentParser(description='# This program trims sequencing file.')

parser.add_argument('-i','--in', dest='IN', help='Input database in .fastq format.')
parser.add_argument('-o','--out', dest='OUT', help='Output file name.')
parser.add_argument('-id', dest='ID', help='ID of a sequence that should be removed.')

args = parser.parse_args()

output = []

for record in SeqIO.parse(args.IN, "fastq"):
    if record.id != args.ID:
        output.append(record)

SeqIO.write(output,args.OUT,'fastq')
