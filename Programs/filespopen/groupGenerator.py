#!/usr/bin/env python3

import sys, os
from Bio import SeqIO

# User input parameters
fa_path = sys.argv[1]
out_name = sys.argv[2]

outVar = []

for record in SeqIO.parse(fa_path, "fasta"):
  #recName = record.id.split(";")[0]
  outVar.append(" ".join([record.id,"A"]))
  
outVar = "\n".join(outVar)
  
output_handle = open(out_name, "w")

output_handle.write(outVar)

output_handle.close()