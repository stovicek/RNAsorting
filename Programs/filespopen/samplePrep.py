#!/usr/bin/env python3.4
#import everything that I will use
import sys, os
from subprocess import Popen
from Bio import SeqIO
import subprocess
#set the directory and order the file by name
os.chdir('./files')
filesList = os.listdir()
filesList = sorted(filesList)

os.makedirs('output')

#print(filesList)
#assigning fw and rev to R1 and R2 seqs
i = 1
for fileName in filesList:
    # if i is an even number
    #print(fileName)
    if fileName[8:10] == "R1":
        forward = fileName
    else:
        reverse = fileName
    # For ever odd loop, we check whether the two file names are identical.
    # In other words, whether we are merging the same files together.
    if i %2 == 0:
        if reverse[0:7] == forward[0:7]:
            #print("Checked!")
            p = Popen(['usearch8', '-fastq_mergepairs', forward, '-reverse', reverse, '-fastq_merge_maxee', '0.5', '-fastqout', "".join(['output/',forward[0:7],".fastq"])],shell = False)
            p.wait()
        else:
            #print("Error!")
            break
    i += 1

#####

os.chdir('./output')
filesList = os.listdir()
os.makedirs('../renamed')
os.makedirs('../discarded')
os.makedirs('../good')
#modifying the code of each sequence, removing most of it and adding
for fileName in filesList:
    #fileName = 'Osnat57.fastq'
    #fileLength = int(check_output(["wc", "-l", fileName]).split()[0])
    #len(str(fileLength))
    output = []
    i = 1
    for record in SeqIO.parse(fileName, "fastq"):
        record.id = ''.join(['seq',str(i),';barcodelabel=',fileName[0:7]])
        record.name = ''
        record.description = ''
        #print(record.description)
        output.append(record)
        i += 1
    SeqIO.write(output,''.join(['../renamed/',fileName[0:7],'_renamed.fastq']),'fastq')
#to remove those seqs that are smaller than 400 nt and to see how many we discarde:
    usearchCommand = ['usearch8', '-fastq_filter', ''.join(['../renamed/',fileName[0:7],'_renamed.fastq']), '-fastq_minlen', '400', '-fastaout', "".join(['../good/',fileName[0:7],".fasta"]), '-fastaout_discarded', "".join(['../discarded/',fileName[0:7],'.fasta'])]
    p = Popen(usearchCommand,shell = False)
    p.wait()

os.chdir('../renamed')
filesList = os.listdir()

print("Now you need to concatenate files with 'cat Osnat* > output.fasta' ")
#print("Replace '@ ' at the beginning of the file with '>', using sed '/^@/s/\@\ /\>/ input.fastq > output.fastq")
