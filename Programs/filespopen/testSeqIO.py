import sys, os
from subprocess import Popen
from Bio import SeqIO
import subprocess

os.chdir('./files')

os.chdir('./output')
filesList = os.listdir()
os.makedirs('../renamed')
os.makedirs('../discarded')
os.makedirs('../good')
#modifying the code of each sequence, removing most of it and adding
for fileName in filesList:
    #fileName = 'Osnat57.fastq'
    #fileLength = int(check_output(["wc", "-l", fileName]).split()[0])
    #len(str(fileLength))
    output = []
    i = 1
    for record in SeqIO.parse(fileName, "fastq"):
        record.id = ''.join(['seq',str(i),';barcodelabel=',fileName[0:7]])
        record.name = ''
        record.description = ''
        #print(record.description)
        output.append(record)
        #to remove those seqs that are smaller than 100 nt and to see how many we discarde:
        #-fastq_filter reads.fastq -fastq_minlen 100 -fastaout_discarded discarded.fasta
        i += 1
        print(record)
        break
    SeqIO.write(output,''.join(['../renamed/',fileName[0:7],'_renamed.fastq']),'fastq')
    break
    usearchCommand = ['usearch8', '-fastq_filter', ''.join(['../renamed/',fileName[0:7],'_renamed.fastq']), '-fastq_minlen', '400', '-fastqout', "".join(['../good/',fileName[0:7],".fastq"]), '-fastqout_discarded', "".join(['../discarded/',fileName[0:7],'.fastq'])]
    p = Popen(usearchCommand,shell = False)
    p.wait()

os.chdir('../renamed')
filesList = os.listdir()

print("Now you need to concatenate files with 'cat Osnat* > output.fastq' ")
print("Replace '@ ' at the beginning of the file with '>', using sed '/^@/s/\@\ /\>/ input.fastq > output.fastq")
