#!/usr/bin/env python3.4
#import everything that I will use
import sys, os
from subprocess import Popen
from Bio import SeqIO
import subprocess

os.chdir('files/good/')
filesList = os.listdir()
os.makedirs('../dereplicated/')
for fileName in filesList:
    usearchCommand=['usearch8', '-derep_fulllength', fileName, '-fastaout', ''.join(['../dereplicated/',fileName[0:7],'_uniques.fasta']), '-sizeout']
    p = Popen(usearchCommand,shell = False)
    p.wait()
    
print("Now you need to concatenate files with 'cat Osnat* > output.fasta' ")