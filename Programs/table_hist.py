import argparse
import re
import csv

parser = argparse.ArgumentParser(description='# This program parses a cmsearch table, retrieves relevant sequences from a supplemented sequence file and saves  them in an output file.')

parser.add_argument('-t','--table', dest='TAB', help='Input table of assigned sequences.')
parser.add_argument('-o','--out', dest='OUT', help='Output file name.')

args = parser.parse_args()

seq_dictionary = {}

output = []

i = 0
with open(args.TAB, 'r') as input_handle:
    for line in input_handle:
        # Leafs through the header
        if i < 16:
            i += 1
            continue
        if line[1] == '-':
            break
        line = line.split()
        #print(float(line[2]) < 0.001)
        output.append(float(line[2]))
        seq_dictionary[line[5]] = float(line[2])
        i += 1

with open(args.OUT, 'w') as output_handle:
    for item in output:
        output_handle.write("%s\n" % item)
