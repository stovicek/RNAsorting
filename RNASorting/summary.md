# Extracting different taxonomic cathegories from the RNA sequencing files

The program `cmscan` is used to search queries from the sequencing file against a covariance model database. This is in order to estimate which sequences likely originate from the analysed groups (e.g. _Bacteria_, _Archaea_, _Eukaryotes_). Both long ribosomal subunit (LSU) and long ribosomal subunit (SSU) are screened for.

---

## Cluster command

The `qsub` command:

```Shell
qsub -cwd -e results/ssu_eukarya/ssu_eukarya_r22.e -o /dev/null -q bioinfo.q@sge102 -m beas -M no.drux@gmail.com infernal.sh
```

```Shell
#!/bin/bash
89 --cpu 15 --anytrunc -o OUTPUT --tblout OUTPUT /fastspace/users/stovicek/XXX/SSU_rRNA_bacteria.cm
```
A custom made [python program](../Programs/infernal.sh) was written in order to allow for the parallel threading.  

```Shell
#!/bin/bash
#$ -M no.drux@gmail.com
#$ -m beas

source ~/.cshrc

sed -n '1~4s/^@/>/p;2~4p' ~/fastspace/totalRNA2015/R2_trimmed3.fastq > ~/fastspace/totalRNA2015/R2_trimmed3.fasta

python3.5 ~/fastspace/totalRNA2015/infernal_singlecore.py --in R2_trimmed3.fasta --out r2 --outpath results/ssu_eukarya/ --cm databases/ssu_eukarya/SSU_rRNA_eukarya.cm --cpu 1
```
### Explanation of the options

|Option name | Description of the function                                                    |
|:-----------| :----------------------------------------------------------------------------- |
| --anytrunc | Allows truncated hits to beging or end at any position in the target sequence. |
| -o | Output file |
| --tblout | Table output that is easy to parse |

## Progress

[x] SSU Eukaryotes
  - [x] Sample 1
  - [x] Sample 2
  - [x] Sample 3
  - [x] Sample 4

[x] SSU Bacteria
  - [x] Sample 1
  - [x] Sample 2
  - [x] Sample 3
  - [x] Sample 4

[x] SSU Archaea
  - [x] Sample 1
  - [x] Sample 2
  - [x] Sample 3
  - [x] Sample 4

[x] SSU Microsporidia
  - [x] Sample 1
  - [x] Sample 2
  - [x] Sample 3
  - [x] Sample 4

[ ] LSU Eukaryotes
  - [ ] Sample 1
  - [ ] Sample 2
  - [ ] Sample 3
  - [ ] Sample 4

[ ] LSU Bacteria
  - [ ] Sample 1
  - [ ] Sample 2
  - [ ] Sample 3
  - [ ] Sample 4


[ ] LSU Archaea
  - [ ] Sample 1
  - [ ] Sample 2
  - [ ] Sample 3
  - [ ] Sample 4

[ ] LSU Microsporidia
  - [ ] Sample 1
  - [ ] Sample 2
  - [ ] Sample 3
  - [ ] Sample 4
