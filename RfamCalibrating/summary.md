# Calibrating of the E-values for the fetched databases

The obtained databases need to be calibrated in order to estimate the proper E-value for the alignments. This is done with the program `cmcalibrate` from the package Infernal.

The following command was passed to the queue on the cluster with the `qsub` method:

---

```Shell
qsub -cwd -e ssu_bacteria.e -o ssu_bacteria.o -q bioinfo.q@sge180 -m abe -M no.drux@gmail.com shell.sh
```
The shell file passed to the `qsub` has a following content:

```Shell
#!/bin/bash
cmcalibrate --cpu 10 /fastspace/users/stovicek/infernal/Rfam/SSU_rRNA_bacteria.cm
```
### The following databases have been calibrated:

- [x] LSU_rRNA_bacteria.cm
- [x] LSU_rRNA_archaea.cm
- [ ] LSU_rRNA_eukarya.cm

- [x] SSU_rRNA_bacteria.cm
- [x] SSU_rRNA_archaea.cm
- [x] SSU_rRNA_eukarya.cm
- [x] SSU_rRNA_microsporidia.cm

---

### An easy way to check the memory status of the bioinfo.q computers

```Shell
qhost | egrep 'sge102|sge176|sge177|sge178|sge180|sge213|sge214|sge224'
```
