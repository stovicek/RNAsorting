# Parsing of the Rfam database

The [Rfam curated database](http://rfam.xfam.org/) of alignments with secondary structure information is used in this project. The database is described in detail [here](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC165453/). Specifically the **version 12.0** of the Rfam database was [downloaded](ftp://selab.janelia.org/pub/rfam/rfam-12.0/Rfam.cm.gz) and parsed for this project.

---

A program `cmfetch` was used in order to retrieve relevant rRNA databases.

```Shell
cmfetch Rfam.cm SSU_rRNA_archaea > SSU_rRNA_archaea.cm
```

Following databases were retrieved:

1. LSU_rRNA_bacteria.cm
2. LSU_rRNA_archaea.cm
3. LSU_rRNA_eukarya.cm

4. SSU_rRNA_bacteria.cm
5. SSU_rRNA_archaea.cm
6. SSU_rRNA_eukarya.cm
7. SSU_rRNA_microsporidia.cm

Those databases can be found currently on the cluster only.
