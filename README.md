# RNA sorting into taxonomic groups

The aim of this project is to sort the ribosomal portion of the total RNA sequencing file into different taxonomic cathegories. This is done by the secondary structure aware alignment tool **Infernal v1.1.1**.

---

### Following is a summary of the operation procedure:

1. Preparation of the reference databases.
  - [Parsing of the Rfam database.](RfamParsing/summary.md)
  - [Calibrating of the parsed Rfam sub-databases](RfamCalibrating/summary.md)
  - [Compressing the reference database](RfamCompressing/summary.md)
2. [Cleaning sequencing files](Sequence preparation/summary.md)
  - Removing of contaminant sequencing adapters
  - Cropping first 12 nucleotides from each read
3. [Sorting of the sequencing file against different databases.](RNASorting/summary.md)
  - _Bacteria_
  - _Archaea_
  - _Eukaryota_

## Versions of the programs used in this project

A list of programs and their versions can be found [here](programVersions.md).  
