# This file contains a list of programs and their versions used in this project

1. [Infernal v1.1.1](http://infernal.janelia.org/)
  - [The publication describing this release.](http://bioinformatics.oxfordjournals.org/content/29/22/2933)
