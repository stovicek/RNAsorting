# Sequencing files quality check and filtering

Each file was checked with the [FastQC](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/) quality analysing program. The resulting files cannot be displayed within gitlab, but they can be obtained from the [following folder](fastqc).

## Trimming procedure

Adapters contamination was determined with the FastQC software and the contaminants were removed with the [cutadapt v1.9.1](https://cutadapt.readthedocs.org/en/stable/) program. The resulting reads were trimmed with a custom made [python program](../Programs/seq_remove.py). The first 12 bases were removed in order to avoid random priming bias and reads that were at the end shorter then 50 bp after the adapter trimming were further discarded.  

## Basic statistics for each sample

|          | Cutadapt process log | Number of sequences before trimming | Percentage of sequences contaminated with adapters | Number of sequences after trimming | Survival |
|:---------| :--------| :-------- | :----| :---------| :-----|
| Sample 1 | [Log](cutadapt/R1.txt) | 5 879 791 | 4.6% | 5 756 929 | 97.9% |
| Sample 2 | [Log1](cutadapt/R2.txt), [Log2](cutadapt/R2_2.txt)| 3 670 926 | 13.2% + 1.7%** | 3 385 053 | 92.2% |
| Sample 3 | [Log](cutadapt/R3.txt) | 4 214 284 | 7.3% | 4 009 803 | 95.1% |
| Sample 4 | [Log1](cutadapt/R4.txt), [Log2](cutadapt/R4_2.txt) | 2 422 696 | 50.0% + 10.1%** | 1 156 483 | 47.7% |

** Two rounds of trimming were necessary in order to remove all detected adapters.
